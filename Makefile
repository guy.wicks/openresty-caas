.SLIENT:
.PHONEY:	build

default: help

help:
	@echo Help

build: _bundle
	docker build --rm --tag openresty/aviva:1.0 .
	docker images

_bundle: _bundle.7z
	7z x _bundle.7z

run: build
	docker run --name aviva-caas -p 8010:80 -p 8005:8005 -p 8006:8006 --rm openresty/aviva:1.0

debug: build
	- docker stop aviva-caas-debug
	- docker rm aviva-caas-debug
	- docker run -d --name aviva-caas-debug -P --rm openresty/aviva:1.0
	- docker exec aviva-caas-debug nginx -t
	- docker exec aviva-caas-debug nginx -T
	- docker exec aviva-caas-debug ls -R /usr/local/openresty/nginx/

stop:
	- docker stop aviva-caas
	- docker stop aviva-caas-debug

rm: stop
	- docker rm aviva-caas
	- docker rm aviva-caas-debug

prune: stop
	- docker system prune --all --force

start:
	docker start aviva-caas

