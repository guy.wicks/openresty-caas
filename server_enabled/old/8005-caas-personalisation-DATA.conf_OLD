server {
    listen          8005;
    server_name     localhost;

    include         ../server_enabled/lua-settings.inc;
    set             $framework_ver                  "v.4.7.0";
    set             $framework_api_root             "1.0";

    #
    # Direct browse to the template samples
    #
    location = / {
        rewrite ^ /intermediary redirect;
    }

    location / {
        root                    ./html;
        ssi	                    on;
        set	                    $inc                $request_uri;
        #set                     $site               adviser;
        #set                     $product_url        $site-$productName;

        add_header              x-inc               $inc;
        add_header              x-uri               $request_uri;
        add_header              x-req               $request_filename;
        add_header              x-doc               $document_root$inc.html;		
        add_header              x-root              $realpath_root$inc.html;		
        add_header              x-service           $service;
        add_header              x-site              $site;		
        #add_header              x-product_url       $product_url;		

        if (!-f $request_filename) {
            #rewrite ^ /index.html last;
            rewrite ^ /$service.html last;
        }

        if (!-f $document_root$inc.html) {
            return 404;
        }
    }

    location /etc               { return 204; }
    location /assets            { return 204; }
    location /images            { return 204; }
    location /static            { return 204; }
    location /favicon.ico       { return 204; }

    location /models {
        root        /html/caas;
        ssi         on;
        ssi_silent_errors off;
        autoindex   on;
        index       off;
    }
    #
    # Get HTML rendered content from personalised data models
    #
    #location ~ "^/api/v1/framework/(?<frmwrk_ver>[A-Z0-9-.]+)/content/(?<businessContext>[A-Z0-9-]+)/(?<isoLang>[A-Z0-9_]+)/(?<contentModel>[A-Z0-9-]+)/(?<key>[A-Z0-9-]+)$" {
    location ~ "^/api/v1/framework/(?<frmwrk_ver>[A-Z0-9-.]+)/content/(?<businessContext>[A-Z0-9-]+)/(?<isoLang>[A-Z0-9_]+)/(?<contentModel>[A-Z0-9-]+)/(?<key>[A-Z0-9-]+)$" {
        add_header  x-request-uri       $request_uri;
        add_header  x-frmwrk_ver        $frmwrk_ver;        
        add_header  x-businessContext   $businessContext;
        add_header  x-isoLang           $isoLang;
        add_header  x-contentModel      $contentModel;
        add_header  x-key               $key;
        add_header  x-content_proxy     $content_proxy;
        add_header  x-template_proxy    $template_proxy;
        add_header  x-request_time      $request_time;

        #add_header  Content-Type        text/html;  
        add_header  Content-Type        text/plain;    # used for debugging

        # Combine the JSON with the HTML
        set $content_proxy              "/api/v1/personalise/$businessContext/$isoLang/$contentModel/$key?$args";
        set $template_proxy             "/api/v1/template/$frmwrk_ver/$businessContext/$isoLang/$contentModel/$key?$args";
        content_by_lua_file             /lua/template-render.lua;

        # TODO: Data redaction
        # - scope_include
        # - scope_exclude (done)
        # - publishing date
        expires                         -1;
    }

    #
    # Add personalised details to the JSON model data (calculate the mustache macros)
    #
    # curl -i "localhost:8005/1.0/personalise/test-data/en_GB/personalise/test1?personalisation=%7B%22firstname%22%3A%22Guy%22%2C%22surname%22%3A%22Wicks%22%7D&scope=club110"
    #location ~ "^/1.0/personalise/(?<businessContext>[A-Z0-9-]+)/(?<isoLang>[A-Z0-9_]+)/(?<contentModel>[A-Z0-9-]+)/(?<key>[A-Z0-9-]+)$" {
    location ~ "^/api/v1/personalise/(?<businessContext>[A-Z0-9-]+)/(?<isoLang>[A-Z0-9_]+)/(?<contentModel>[A-Z0-9-]+)/(?<key>[A-Z0-9-]+)$" {
        add_header              x-request-uri       $request_uri;
        add_header              x-request_time      $request_time;        
        add_header              Content-Type        application/json;        
        set $personalisation    $arg_personalisation;
        set $scope              $arg_scope;
        set $content_proxy      "/1.0/models/$businessContext/$isoLang/$contentModel/$key";

        content_by_lua_file     /lua/template-personalise.lua;
    }

    #
    # Get the raw data model from AEM
    #
    location ~ "^/1.0/models/(?<businessContext>[A-Z0-9-]+)/(?<isoLang>[A-Z0-9_]+)/(?<contentModel>[A-Z0-9-]+)/(?<key>[A-Z0-9-]+)$" {
        add_header  x-request-uri       $request_uri;        
        add_header  x-businessContext   $businessContext;
        add_header  x-isoLang           $isoLang;
        add_header  x-contentModel      $contentModel;
        add_header  x-key               $key;
        add_header  x-request_time      $request_time;
        add_header  x-uri-1             /html/caas/models/$businessContext/$contentModel/$key.json;
        add_header  x-uri-1             /html/caas/models/$businessContext/$contentModel.json;

        # TODO: Wire up to AEM content model service
        # proxy_cache
        # proxy_pass https://aem_caas_service/$businessContext/$isoLang/$contentModel/$key;

        # TEMP -for now, just get static content models directly
        #alias       /html/caas/models/$businessContext;
        alias       /models/$businessContext;
        try_files   /$contentModel/$key.json /$contentModel.json = 404;

        expires     -1;
    }

    #
    # Get the HTML template (framework dependent)
    #
    # curl -i http://localhost:8005/1.0/template/connect/en_GB/product-card/my-product-card
    # curl -i http://localhost:8005/v.3.4.2/template/connect/en_GB/product-card/my-product-card
    # curl -i http://localhost:8005/v.4.7.0/template/intermediary/en_GB/navigation-masthead/broker
    #location ~ "^/1.0/template/(?<businessContext>[A-Z0-9-]+)/(?<isoLang>[A-Z0-9_]+)/(?<contentModel>[A-Z0-9-]+)/(?<key>[A-Z0-9-]+)$" {
    
    #location ~ "^/(?<frmwrk_ver>[A-Z0-9-.]+)/template/(?<businessContext>[A-Z0-9-]+)/(?<isoLang>[A-Z0-9_]+)/(?<contentModel>[A-Z0-9-]+)/(?<key>[A-Z0-9-]+)$" {
    location ~ "^/api/v1/template/(?<frmwrk_ver>[A-Z0-9-.]+)/(?<businessContext>[A-Z0-9-]+)/(?<isoLang>[A-Z0-9_]+)/(?<contentModel>[A-Z0-9-]+)/(?<key>[A-Z0-9-]+)$" {
        add_header  x-request-uri       $request_uri;
        add_header  x-frmwrk_ver        $frmwrk_ver;
        add_header  x-businessContext   $businessContext;
        add_header  x-isoLang           $isoLang;
        add_header  x-contentModel      $contentModel;
        add_header  x-key               $key;
        add_header  x-request_time      $request_time;

        # Use the version specific template or fail back to a generic base framework version
        #alias       /html/caas/framework/;
        alias       /framework/;
        try_files   /$frmwrk_ver/$contentModel.html = 201; # /v4.7.0/$contentModel.html /v.4.0.0/$contentModel.html = 404;
    }


    #
    # Data integration
    #
    #
    # Get HTML rendered content from personalised data models
    #
    # curl -i http://localhost:8005/1.0/framework/v.4.5.2/data/adviser/en_GB/document-library/2332
    location ~ "^/1.0/framework/(?<frmwrk_ver>[A-Z0-9-.]+)/data/(?<businessContext>[A-Z0-9-]+)/(?<isoLang>[A-Z0-9_]+)/(?<contentModel>[A-Z0-9-]+)/(?<key>[A-Z0-9-]+)$" {
        add_header  x-request-uri       $request_uri;
        add_header  x-businessContext   $businessContext;
        add_header  x-isoLang           $isoLang;
        add_header  x-contentModel      $contentModel;
        add_header  x-key               $key;
        add_header  x-content_proxy     $content_proxy;
        add_header  x-request_time      $request_time;

        add_header  Content-Type        text/html;  
        #add_header  Content-Type        text/plain;    # used for debugging

        # Combine the JSON with the HTML
        #set $content_proxy              "/1.0/personalise/$businessContext/$isoLang/$contentModel/$key?personalisation=%7B%22firstname%22%3A%22Guy%22%2C%22surname%22%3A%22Wicks%22%7D&scope=broker";
        set $personalisation            "%7B%22firstname%22%3A%22Guy%22%2C%22surname%22%3A%22Wicks%22%7D";
        set $scope                      "broker";
        
        set $content_proxy              "/1.0/data/$businessContext/$isoLang/$contentModel/$key";
        set $template_proxy             "/$frmwrk_ver/template/$businessContext/$isoLang/$contentModel/$key?$args";
        content_by_lua_file             /lua/template-render.lua;
        
        expires                         -1;
    }

    #location ~ "^/1.0/data/(?<businessContext>[A-Z0-9-]+)/(?<isoLang>[A-Z0-9_]+)/document-manager/(?<key>[A-Z0-9-]+)$" {
    # curl -i http://localhost:8005/1.0/data/broker/en_GB/document-library/2332
    location ~ "^/1.0/data/broker/en_GB/document-library/(?<key>[A-Z0-9-]+)$" {
        add_header              x-request-uri       $request_uri;
        add_header              x-businessContext   broker;
        add_header              x-productCode       $arg_key;
        add_header              x-requestTime       $request_time;
        #add_header              x-endpoint          $api_end_point;

        proxy_pass              http://content_manager/api/v1/publicProductDocumentDetails/broker?productCode=$key;
    }

    #location ~ "^/1.0/data/(?<businessContext>[A-Z0-9-]+)/(?<isoLang>[A-Z0-9_]+)/document-manager/(?<key>[A-Z0-9-]+)$" {
    # curl -i http://localhost:8005/1.0/data/adviser/en_GB/document-manager/2332
    location ~ "^/1.0/data/(?<businessContext>[A-Z0-9-]+)/en_GB/document-library/(?<key>[A-Z0-9-]+)$" {
        add_header              x-request-uri       $request_uri;
        add_header              x-businessContext   $businessContext;
        add_header              x-productCode       $arg_key;
        add_header              x-requestTime       $request_time;
        #add_header              x-endpoint          $api_end_point;

        proxy_pass              http://document_manager/api/v1/publicProductDocumentDetails/$businessContext?productCode=$key;
    }

        location ~ "^/api/v1/data/(?<businessContext>[A-Z0-9-]+)/en_GB/document-library/(?<key>[A-Z0-9-]+)$" {
        add_header              x-request-uri       $request_uri;
        add_header              x-businessContext   $businessContext;
        add_header              x-productCode       $arg_key;
        add_header              x-requestTime       $request_time;
        #add_header              x-endpoint          $api_end_point;

        proxy_pass              http://document_manager/api/v1/publicProductDocumentDetails/$businessContext?productCode=$key;
    }

}