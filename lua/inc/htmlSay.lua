--[[

Simple HTML tagging helper
wip

]]
htmlsay = {
  p = function(v)
    return "<p>" .. v .. "</p>"
  end,
  pre = function(v)
    return "<pre>" .. v .. "</pre>"
  end
}

return htmlsay
