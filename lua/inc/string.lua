--[[
Extend the string class

array[] = string.split( string, delimiter )
Split a string into an array

]]
--# Split a string by delimiter into an array
string.split = function(s, delimiter)
  result = {}
  for match in (s .. delimiter):gmatch("(.-)" .. delimiter) do
    table.insert(result, match)
  end
  return result
end

return string
