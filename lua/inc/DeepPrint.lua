--[[
    
]]
local DeepPrint = {}

local function dp( data, i )
    local i = i or 0
    for k, v in pairs( data ) do
      if type(v) == "table" then
          --# Recurse into the table
          print( string.rep("\t",i), k .. " =>" )
          dp(v, i + 1)   
      else
        --# It is just a table attribute
        print( string.rep("\t",i), k, v )
      end
    end
  end

DeepPrint.decode = dp

return DeepPrint