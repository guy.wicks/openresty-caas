--[[
#NGINX OpenResty lua stub

Allows you to run OpenResty lua modules within a normal lua IDE
Mimics or suppresses the nxg object while in IDE mode


Usage: Add to the start of your module

require("inc/nginx-stub")

To determine which mode you are in, so you can possibly add local test data, use

if ngx.stub then
-- you are in stub mode
end

]]
if ngx then
	--#ngx.log(ngx.INFO, "## ngx run-time mode enabled")
else
	print("## ngx.stub mode enabled")

	ngx = {
		--# You can ise this variable to test for stub mode
		stub = true,
		--# Core constants
		OK = 0,
		ERROR = -1,
		AGAIN = -2,
		DONE = -4,
		DECLINED = -5,
		--# ngx.log severity types
		STDERR = "STDERR",
		EMERG = "EMERG",
		ALERT = "ALERT",
		CRIT = "CRIT",
		ERR = "ERR",
		WARN = "WARN",
		NOTICE = "NOTICE",
		INFO = "INFO",
		DEBUG = "DEBUG",
		--# HTTP status codes
		HTTP_OK = 200,
		HTTP_BAD_REQUEST = 400,
		HTTP_NOT_FOUND = 404,
		--# HTTP Verbs
		HTTP_GET = "HTTP_GET",
		HTTP_HEAD = "HTTP_HEAD",
		HTTP_PUT = "HTTP_PUT",
		HTTP_POST = "HTTP_POST",
		HTTP_DELETE = "HTTP_DELETE",
		--# NGINX internal variables
		var = {
			template_proxy = "",
			content_proxy = ""
		},
		location = {
			capture = function()
				return {body = "", status = 0}
			end
		},
		--#
		log = function(s, v)
			print("ngx.log:", s, v)
		end,
		--#
		print = function(...)
			print("ngx.print:", s, v)
		end,
		--#
		say = function(v)
			print("ngx.say:", v)
		end,
		--#
		exit = function(v)
			print("ngx.exit:", v)
			os.exit()
		end
	}

	return ngx
end
