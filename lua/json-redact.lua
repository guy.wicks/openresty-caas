local inspect = require("inc/inspect")

--# Create a class
local redact = {}

--#
--# PRIVATE METHODS
--#

--# Do any of the values in 'scope_val' match 'data_val'
local function array_match(data_arr, scope_arr)
  for dk, dv in ipairs(data_arr) do
    for sk, sv in ipairs(scope_arr) do
      if dv == sv then
        return true
      end
    end
  end
  return false
end

--#
--# PUBLIC METHODS
--#

--# NB, it is (still) clobbering the original data variable!
redact.remove = function (data, scope)
  --# Scan the table to remove if non-applicable scopes
  for data_key, data_val in pairs(data) do
    if type(data_val) == "table" then
      --# Resurse into the table, deep first
      redact.remove(data_val, scope)

      --# If the attribute value matches a 'scope' then redact
      for scope_key, scope_val in pairs(scope) do
        --# Snoop into this object and see if there is a scope defined
        if data_val[scope_key] then
          if not array_match(data_val[scope_key], scope_val) then
            data[data_key] = {_redacted = {"no matching scopes"}}
          end
        end
      end
    end
  end
end

return redact
