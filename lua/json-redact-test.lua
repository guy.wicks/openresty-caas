local inspect = require("inc/inspect")
local redact = require("json-redact")

local test1_data = {
    d = {
        heading = "Club 110 content",
        applicable_to = {"club110"}
    }
}

local scope1_include = {applicable_to = {"club110"}}
redact.remove(test1_data, scope1_include)
print(inspect(test1_data))

local test2_data = {
    d = {
        heading = "Club 110 content",
        applicable_to = {"club110"}
    }
}

local scope2_include = {applicable_to = {"broker"}}
redact.remove(test2_data, scope2_include)
print(inspect(test2_data))
