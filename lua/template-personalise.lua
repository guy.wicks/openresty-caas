--[[
TODO: Refactor this to separate out better the different phases

NGINX Variables

$content_proxy  - URI to the upstream content template proxy

]]
require("inc/nginx-stub")

local lustache = require("inc/lustache")
local json = require("inc/json4lua")
local inspect = require("inc/inspect")
local url_decode = require("inc/url-decode")
local string = require("inc/string")
local redact = require("json-redact")

ngx.log(ngx.DEBUG, "template-personalise.lua")
ngx.log(ngx.DEBUG, "\n\n" .. string.rep("-", 40) .. "\n")

local template_URI = ngx.var.content_proxy --.. ngx.var.key
local res = ngx.location.capture(template_URI)

--#
--# Local test data
--#
if ngx.stub then
    res.status = ngx.HTTP_OK
    res.body = '{"firstname":"{{firstname}}","scope_include":["club110"]}'
    ngx.var.personalisation = "%7B%22firstname%22%3A%22Guy%22%2C%22surname%22%3A%22Wicks%22%7D"
    ngx.var.scope = "club110"
end

--# If a valid response from upstream
if res.status == ngx.HTTP_OK then
    --# Decode parameter data (%20 etc)
    local personalisation_data = url_decode.decode(ngx.var.personalisation)
    local scope_data = url_decode.decode(ngx.var.scope)
    local output = res.body

    --# TODO: Some parameter verification here please.
    local scope = string.split(scope_data, ",")
    local scope_exclude = {scope_exclude = scope}
    local scope_include = {scope_include = scope}

    --# Check the personalisation JSON is parsable
    if false then
        if pcall(json.decode, personalisation_data) then
            personalisation_data = json.decode(personalisation_data)
        else
            ngx.log(ngx.ERR, "template-personalise.lua:personalisation_data - JSON parse error")
            ngx.log(ngx.INFO, personalisation_data)
            personalisation_data = ""
        end
    end

    --# Check the model JSON is parsable, or throw an error
    local model_json = {}
    if pcall(json.decode, res.body) then
    else
        ngx.log(ngx.ERR, "template-personalise.lua:model_json - JSON parse error")
        ngx.log(ngx.DEBUG, model_json)
        ngx.exit(ngx.HTTP_BAD_REQUEST)
    end

    --# Redact sections that where the scopes should be removed
    model_json = json.decode(res.body)
    redact.remove(model_json, scope_include)

    --# Merge the personalisation data with the model
    --# NOTE: In this 'lustache' implementation, missing tag data will render a blank
    output = lustache:render(json.encode(model_json), personalisation_data)

    ngx.log(ngx.DEBUG, "\n\n" .. string.rep("-", 40) .. "\n")
    ngx.log(ngx.DEBUG, "ngx.var.content_proxy: " .. url_decode.decode(template_URI))
    ngx.log(ngx.DEBUG, "res.body: \n" .. res.body)
    ngx.log(ngx.DEBUG, "ngx.var.scope: \n" .. inspect(scope) .. "\n")
    ngx.log(ngx.DEBUG, "ngx.var.personalisation: \n" .. inspect(personalisation_data) .. "\n")
    ngx.log(ngx.DEBUG, "\noutput: " .. output)

    ngx.say(output)
    ngx.exit(ngx.HTTP_OK)
else
    ngx.exit(res.status)
end
