--[[



NGINX Variables

$content_proxy  - URI to the upstream content template proxy

]]
require("inc/nginx-stub")

local lustache = require("inc/lustache")
local json = require("inc/json4lua")
local inspect = require("inc/inspect")
local htmlEntities = require("inc/htmlEntities")
local html = require("inc/htmlsay")

ngx.log(ngx.DEBUG, "template-render.lua")
ngx.log(ngx.DEBUG, "\n\n" .. string.rep("-", 40) .. "\n")

--# Get the HTML template
local template_URI = ngx.var.template_proxy
local template_res = ngx.location.capture(template_URI)

--# Get the JSON content
local content_URI = ngx.var.content_proxy
local content_res = ngx.location.capture(content_URI)

--#
--# Local test data
--#
if ngx.stub then
    template_res.status = ngx.HTTP_OK
    template_res.body = '{"firstname":"{{firstname}}"}'
    content_res.status = ngx.HTTP_OK
    content_res.body = '{"firstname":"{{firstname}}"}'
end

--# As long as both of the above worked...
--# ...and the JSON is valid
--# ...then combine the JSON with the HTML template (with the mustache macros)
if template_res.status == ngx.HTTP_OK and content_res.status == ngx.HTTP_OK then
    ngx.log(ngx.DEBUG, "template_res.body\n" .. template_res.body)
    ngx.log(ngx.DEBUG, "content_res.body\n" .. content_res.body)
    if pcall(json.decode, content_res.body) then
        local content_JSON = json.decode(content_res.body)

        output = htmlEntities.decode(lustache:render(template_res.body, content_JSON))
        ngx.log(ngx.DEBUG, "output\n" .. output)
        -- Remove comments
        -- Specific use-case to un-quote embedded JSON
        output = output:gsub("//{", "{")

        -- Remove the mustache variable comments
        output = output:gsub("[\r\n]+%s*//[^\n]*", "")

        -- Remove human // comments
        output = output:gsub("^//[^\n]*", "")

        -- Remove trailing ,{}'s
        output = output:gsub(",%s*{}", "")

        -- nasty hack to fix embedded JSON in the AEM JCR that has the wrong attribute names
        output = output:gsub('"link":', '"url":')
        output = output:gsub('"text":', '"title":')
        output = output:gsub('"linkText":', '"title":')
        output = output:gsub('"anchor":"",', "")
        output = output:gsub(',"showExternalIcon":false', "")

        ngx.say(output)
        ngx.exit(ngx.HTTP_OK)
    else
        ngx.log(ngx.ERR, "Error while decoding {content_res.body}")
    end
end

--# Something went wrong above: throw 400 - Bad Request
--# - either a HTML template did not load
--# - or the JSON did not load
--# - or the JSON did not parse
ngx.log(ngx.ERR, template_res.status .. " - " .. template_URI)
ngx.log(ngx.DEBUG, "template_res.body: " .. template_res.body)
ngx.log(ngx.ERR, content_res.status .. " - " .. content_URI)
ngx.log(ngx.DEBUG, "content_res.body: " .. content_res.body)
ngx.exit(ngx.HTTP_NOT_FOUND)

ngx.say(html.pre(template_res.status .. " x " .. template_URI))
ngx.say(html.pre(content_res.status .. " y " .. content_URI))
