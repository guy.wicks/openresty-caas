require("inc/nginx-stub")
local json = require("inc/json4lua")
local xml = require("inc/simpleXML").newParser()
local inspect = require("inc/inspect")

--#
--# External (NGINX) Paramters
--#
local source_URI = ngx.var.source_URI
local res = ngx.location.capture(source_URI)
--
local businessContext = ngx.var.businessContext
local filter_productCode = ngx.var.productCode
local filter_documentRef = ngx.var.documentRef
local hostname = "http://localhost:8005"
local root_URL = string.format("%s://%s:%s%s", ngx.var.scheme, ngx.var.host, ngx.var.server_port, ngx.var.document_uri)

--#
--#
--#
if res.status ~= ngx.HTTP_OK then
    ngx.exit(res.status)
end

--#
--#
--#
local documents_xml = xml:ParseXmlText(res.body)
local found = 0

--#
--# Start the JSON output stream
--#
ngx.say('{"documents":[')
for k, v in ipairs(documents_xml:children()) do
    --# <structure>
    for k, v in ipairs(v:children()) do
        --# <proposition>
        for k, v in ipairs(v:children()) do
            --# <page>
            local productCode = v["@id"]
            local productName = v["@title"]
            for k, v in ipairs(v:children()) do
                --# <section>
                local documentType = v["@title"]
                for k, doc in pairs(v:children()) do
                    --# <document>
                    local documentDescription = doc["description"]:value()
                    local documentRef = doc["@reference"]
                    local documentId = doc["@id"]
                    local effectiveDate = doc["published"]:value()
                    if
                        (filter_documentRef == documentRef and filter_productCode == productCode) or
                            (filter_documentRef == nil and filter_productCode == productCode)
                     then
                        --# Not the most efficient way, I would expect. Should emit with a single "say"
                        ngx.say('{"documentDescription":"' .. documentDescription .. '",')
                        ngx.say('"documentRef":"' .. documentRef .. '",')
                        ngx.say('"documentType":"' .. documentType .. '",')
                        ngx.say('"fileType":"' .. "PDF" .. '",')
                        ngx.say('"fileSize":"' .. "200 kb" .. '",')
                        ngx.say('"mimetype":"' .. "application/PDF" .. '",')
                        ngx.say('"productCode":"' .. productCode .. '",')
                        ngx.say('"productName":"' .. productName .. '",')
                        ngx.say('"effectiveDate":"' .. effectiveDate .. '",')
                        ngx.say('"printOnly":' .. "false" .. ",")
                        ngx.say('"_links":{')

                        ngx.say(
                            '"self":"' ..
                                hostname ..
                                    "/publicProductDocuments/" ..
                                        businessContext .. "/" .. documentRef .. "?productCode=" .. productCode .. '",'
                        )
                        ngx.say('"href":"https://www.aviva.co.uk/documents/view/al55011c.pdf"')
                        ngx.say("}")
                        ngx.say("},")

                        found = found + 1
                    end
                end
            end
        end
    end
end

ngx.say("{}]}")
if found == 0 then
    ngx.exit(ngx.HTTP_NOT_FOUND)
else
    ngx.exit(ngx.HTTP_OK)
end
