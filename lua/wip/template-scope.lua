--[[

]]
local json = require("json4lua")
local inspect = require("inspect")
local deepprint = require("deepprint")

local json_str =
    [[
        {
            "content": [
                {
                    "fragment": {
                        "heading": "This is a Title1",
                        "subheading": "{{firstname}}",
                        "scope": "club110"
                    }
                },
                {
                    "fragment": {
                        "subheading": "{{firstname}}",
                        "heading": "This is a Title2",
                        "size": "1"
                    }
                }
            ]
        }
]]

function q(s)
    return '"' .. s .. '"'
end

function DeepPrint(e, p, i)
    i = i or 0
    k = k or ""
    p = p or ""

    if type(e) == "table" then
        for k, v in pairs(e) do -- for every element in the table
            --ngx.say(i .. "\t" .. type(e).. "\t" .. k)
            --ngx.say(string.rep(" ",i) .. k .. "\t")
            local scope = (e["scope"] and true) and "true" or "false"

            --ngx.say("hey, found a scope:" .. scope)
            if e["scope"] then
              --  ngx.say("hey, found a scope:" .. e["scope"])
            end

            if tonumber(k) ~= nil then
               r = DeepPrint(v, p .. "[", i + 1) -- recursively repeat the same procedure
            else
               r = DeepPrint(v, p .. "{ " .. q(k) .. ": ", i + 1) -- recursively repeat the same procedure
            end
            return r 
        end
    else -- if not, we can just print it (key=value)

        return p .. q(e) .. " },"

    end
end

--ngx.say(inspect(json.decode(json_str)))
ngx.say(inspect(json.encode(json.decode(json_str))))



ngx.say(inspect(DeepPrint(json.decode(json_str))))
