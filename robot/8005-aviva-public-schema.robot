*** Settings ***
Documentation       Test Suite
...
...     Document Library tests

Library	        Collections			
Library	        RequestsLibrary
Resource        HTTP_Status_Codes.resource
Resource        JSON_Validation.resource
Resource        Variables.resource

Test Template   Validate ContentModel Schema

*** Test Cases ***
# Test name                                     businessContext     contentModel            key
aviva-co.uk:global-head:global-head             aviva-co-uk         global-head             global-head
aviva-co.uk:global-footer:global-footer         aviva-co-uk         global-footer           global-footer
aviva-co.uk:masthead-nav:masthead-nav           aviva-co-uk         masthead-navigation     masthead-navigation
aviva-co.uk:masthead-nav:business               aviva-co-uk         masthead-navigation     business
aviva-co.uk:masthead-selector:masthead-sel      aviva-co-uk         masthead-selector       masthead-selector
aviva-co.uk:global-footer:health                aviva-co-uk         global-footer           health
aviva-co.uk:product-card:health-articles        aviva-co-uk         product-card            health-articles
