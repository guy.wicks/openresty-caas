# Common RESTful HTTP response status codes
*** Variables ***
${HTTP OK}                      200
${HTTP Created}                 201

${HTTP Bad Request}             400
${HTTP Unauthorized}            401
${HTTP Forbidden}               403
${HTTP Not Found}               404
${HTTP Method Not Allowed}      405
${HTTP Not Acceptable}          406
${HTTP Conflict}                409
${HTTP Gone}                    410
${HTTP Unsupported Media Type}  415

*** Keywords ***
HTTP Request Should Be OK
    Should Be Equal As Strings	${Status}  ${HTTP OK}

HTTP Request Should Be Not Found
    Should Be Equal As Strings	${Status}  ${HTTP Not Found}

HTTP Request Should Be Forbidden
    Should Be Equal As Strings	${Status}  ${HTTP Forbidden}   

*** Test Cases ***				
