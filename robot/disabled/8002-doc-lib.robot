*** Settings ***				
Library     Collections			
Library     RequestsLibrary	
Library     JSONSchemaLibrary       robot\schemas

Default Tags    RAAS  CRM-2016  customer-api  server

Documentation  [https://v16l-tfs4.ad.bl.uk/ADS/PLR/_workitems/edit/21026| Story 21026 ]
...  = ISBN Conversion and storage =
...
...  | As a PLR manager I want a book to be recognised whether the ISBNs entered into the system is either 10 or 13 digits. |
...  = ACCEPTANCE CRITERIA =
...  | ISBN service should convert a 10 digit ISBN to a 13 digit ISBN\r |
...  | ISBN service should validate a 10 digit ISBN\r |
...  | ISBN service should validate a 13 digit ISBN\r |
...  [http://git.wa.bl.uk/PLRTestAutomation/api/isbn-service|Git Lab]
...
...  | = Feature = | = As a PLR manager I want a book to be recognised whether the ISBNs entered into the system is either 10 or 13 digits = |
...  | Scenario | An ISBN10 and/or ISBN13 book has been entered into the PLR system |
...  | Given | The LoanRecordValidatorWorker is available |
...  | And | ISBN-Service is installed on the SIT environment |
...  | When | A loan record is validated |
...  | Then | Check ISBN10 or ISBN13 is validated |
...  | And | Check the service can convert ISBN10 to ISBN13 |
...  | And | Check the service can handle & alert the user to invalid ISBN10 and/or ISBN13 |

*** Test Cases ***				
Get Content Manager documentRef
	Create Session	local	http://localhost:8002/document/search/general-insurance-product-documents/documentRef
	${resp}=	Get Request	local	/CTRTG13959
	Should Be Equal As Strings	${resp.status_code}	200	

Get Content Manager productCode
	Create Session	local	http://localhost:8002/document/search/general-insurance-product-documents/productCode
	${resp}=	Get Request	local	/MAST
	Should Be Equal As Strings	${resp.status_code}	200	

Get Content Manager GUID
	Create Session	local	http://localhost:8002/document/search/general-insurance-product-documents/documentGUID
	${resp}=	Get Request	local	/a4de978e-7953-4ab3-87c2-01ad338c58c9
	Should Be Equal As Strings	${resp.status_code}	404
	Log Variables
