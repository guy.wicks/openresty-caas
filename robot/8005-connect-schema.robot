*** Settings ***
Documentation       Test Suite
...
...     Document Library tests

Library	        Collections			
Library	        RequestsLibrary
Resource        HTTP_Status_Codes.resource
Resource        JSON_Validation.resource
Resource        Variables.resource

Test Template   Validate ContentModel Schema

*** Test Cases ***
# Test name                                     businessContext     contentModel            key
#connect:global-head:adviser                     connect             global-head             adviser
#connect:global-head:broker                      connect             global-head             broker
#connect:global-head:health                      connect             global-head             health   
#
#connect:global-footer:adviser                   connect             global-footer           adviser
#connect:global-footer:broker                    connect             global-footer           broker
#connect:global-footer:health                    connect             global-footer           health
#
#connect:masthead-navigation:adviser             connect             masthead-navigation     adviser
#connect:masthead-navigation:broker              connect             masthead-navigation     broker
#connect:masthead-navigation:health              connect             masthead-navigation     health
#
#connect:masthead-selector:adviser               connect             masthead-selector       adviser
#connect:masthead-selector:broker                connect             masthead-selector       broker
#connect:masthead-selector:health                connect             masthead-selector       health
#
#connect:product-card:broker-resources           connect             product-card            broker-resources
#connect:product-card:broker-updates             connect             product-card            broker-updates
#