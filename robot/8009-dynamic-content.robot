*** Settings ***
Documentation       Dynamic Content Service
...
...     Document Library tests

Library	        Collections
Library	        RequestsLibrary
#Library         JSONSchemaLibrary       html/caas/schemas
Resource        JSON_Validation.resource
Resource        HTTP_Status_Codes.resource

Test Template   Validate Fragment

*** Variables ***
#${DynContent Server}            https://dynamicContent.platform.dcuk.sys.aws-euw1-np.avivacloud.com
${DynContent Server}            http://localhost:8005
${api root}                     /api/v1

${auth username}                ConnectUser
${auth password}                C0nnect1ng!

#${framework version}            v.4.7.0
${framework version}            4.7.0


*** Test Cases ***
# Test name                                     businessContext     contentModel            key
#test-data:ref-example                           test-data           ref-example             ref-example
DynContent:001              intermediary            navigation-masthead     adviser
DynContent:002              intermediary            navigation-masthead     broker
DynContent:003              intermediary            navigation-masthead     health

DynContent:004              intermediary            navigation-footer       adviser
DynContent:005              intermediary            navigation-footer       broker
DynContent:006              intermediary            navigation-footer       health

DynContent:007              intermediary            card-icon               resources 
DynContent:008              intermediary            card-editorial          aviva-focus
DynContent:009              intermediary            card-carousel           latest-updates
DynContent:010              intermediary            card-carousel           broker-members-lounge

DynContent:011              intermediary            global-notification     adviser-service-message
DynContent:012              intermediary            global-notification     broker-service-message

#DynContent:101              framework               framework-head          framework-head
DynContent:102              framework               framework-masthead      framework-masthead
#DynContent:103              framework               framework-post          post


*** Keywords ***
Validate Fragment
    [Arguments]                                 ${businessContext}          ${contentModel}     ${key}
                    Get HTML Fragment           ${businessContext}          ${contentModel}     ${key}
                    HTTP Request Should Be OK

                    Get JSON Fragment           ${businessContext}          ${contentModel}     ${key}
                    HTTP Request Should Be OK


Get HTML Fragment
    [Arguments]                                 ${businessContext}          ${contentModel}     ${key}

    &{headers} =    Create Dictionary           X-Aviva-Correlation-ID=wicksg
    ${auth} =       Create List                 ${auth username}            ${auth password}
                    Create Session	    shtml   ${DynContent Server}        auth=${auth}        headers=${headers}      verify=True
    ${resp} =       Get Request	        shtml   ${api root}/framework/${framework version}/content/${businessContext}/en_GB/${contentModel}/${key}
                    Set Test Variable           ${Status}                   ${resp.status_code}
                    Set Test Variable           ${Content}                  ${resp}
                    Set Test Variable           ${contentModel}
                    Set Tags                    bc-${businessContext}       cm-${contentModel}      key-${key}

Get JSON Fragment
    [Arguments]                                 ${businessContext}          ${contentModel}     ${key}

    &{headers} =    Create Dictionary           X-Aviva-Correlation-ID=wicksg
    ${auth} =       Create List                 ${auth username}            ${auth password}
                    Create Session	    sjson   ${DynContent Server}        auth=${auth}        headers=${headers}      verify=True
    ${resp} =       Get Request	        sjson   ${api root}/personalise/${businessContext}/en_GB/${contentModel}/${key}
                    Set Test Variable           ${Status}                   ${resp.status_code}
                    Set Test Variable           ${Content}                  ${resp}
                    Set Test Variable           ${contentModel}
                    Set Tags                    bc-${businessContext}       cm-${contentModel}      key-${key}

