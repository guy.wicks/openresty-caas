*** Settings ***
Documentation	Test Suite
...
...	Document Library tests

Library		Collections	
Library		RequestsLibrary
Resource	HTTP_Status_Codes.resource
Resource	JSON_Validation.resource
Resource	Variables.resource

Test Template	Validate ContentModel Schema

*** Test Cases ***
# Test name											businessContext		contentModel			key

intermediary:card-carousel:latest-updates			intermediary		card-carousel			latest-updates
intermediary:card-carousel:broker-members-lounge	intermediary		card-carousel			broker-members-lounge
intermediary:card-carousel:broker-updates			intermediary		card-carousel			broker-updates

intermediary:card-editorial:aviva-focus				intermediary		card-editorial			aviva-focus
intermediary:card-icon:resources					intermediary		card-icon				resources
intermediary:card-navigation:quick-links			intermediary		card-navigation			quick-links

intermediary:navigation-masthead:adviser			intermediary		navigation-masthead		adviser
intermediary:navigation-masthead:broker				intermediary		navigation-masthead		broker
intermediary:navigation-masthead:health				intermediary		navigation-masthead		health

intermediary:navigation-footer:adviser				intermediary		navigation-footer		adviser
intermediary:navigation-footer:broker				intermediary		navigation-footer		broker
intermediary:navigation-footer:health				intermediary		navigation-footer		health

intermediary:navigation-selector:adviser			intermediary		navigation-selector		adviser
intermediary:navigation-selector:broker				intermediary		navigation-selector		broker
intermediary:navigation-selector:health				intermediary		navigation-selector		health

#connect:global-head:adviser	connect	global-head	adviser
#connect:global-head:broker	connect	global-head	broker
#connect:global-head:health	connect	global-head	health	
#
#connect:global-footer:adviser	connect	global-footer	adviser
#connect:global-footer:broker	connect	global-footer	broker
#connect:global-footer:health	connect	global-footer	health
#
#connect:navigation-masthead:adviser	connect	navigation-masthead	adviser
#connect:navigation-masthead:broker	connect	navigation-masthead	broker
#connect:navigation-masthead:health	connect	navigation-masthead	health
#
#connect:masthead-selector:adviser	connect	masthead-selector	adviser
#connect:masthead-selector:broker	connect	masthead-selector	broker
#connect:masthead-selector:health	connect	masthead-selector	health
#
#connect:product-card:broker-resources	connect	product-card	broker-resources
#connect:product-card:broker-updates	connect	product-card	broker-updates
#