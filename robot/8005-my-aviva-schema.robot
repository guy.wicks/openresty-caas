*** Settings ***
Documentation       Test Suite
...
...     Document Library tests

Library	        Collections			
Library	        RequestsLibrary
Resource        HTTP_Status_Codes.resource
Resource        JSON_Validation.resource
Resource        Variables.resource

Test Template   Validate ContentModel Schema

*** Test Cases ***
# Test name                                     businessContext     contentModel            key
my-aviva:card-navigation:offers                 my-aviva            card-navigation         offers
my-aviva:card-navigation:offers-all             my-aviva            card-navigation         offers-all
