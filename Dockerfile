#FROM openresty/rpi:latest
FROM openresty/openresty:latest

COPY nginx.conf                               /usr/local/openresty/nginx/conf
COPY _bundle/server_enabled                   /usr/local/openresty/nginx/server-enabled
COPY _bundle/server_enabled/lua-settings.inc  /usr/local/openresty/nginx/conf
COPY _bundle/html                             /usr/local/openresty/nginx/html
COPY _bundle/lua                              /usr/local/openresty/nginx/lua

#RUN ls -laR /etc/nginx
RUN ls -laR /usr/local/openresty/nginx
RUN mkdir -p /temp/cache/aem
#RUN cat /usr/local/openresty/nginx/conf/nginx.conf
#RUN ls -laR /etc/nginx/conf.d/*.conf
#RUN rm /etc/nginx/conf.d/default.conf
#RUN ls -laR /etc/nginx/server-enabled
